import $ from 'jquery';
// import './tools/svg4everybody';
// import { media } from './tools/MQ';

// Components

// content load components
const componentsload = [];

// once delegated components
const components = [].concat(componentsload);

window.App = {
	run() {
		// console.log(media('lgDown'));
		// console.log(media('webkit'));

		const $target = $(document);
		components.forEach((component) => component.init($target));

		$(document).on('contentload', (event) => {
			const $eventTarget = $(event.target);
			componentsload.forEach((component) => component.init($eventTarget));
		});

		//checkbox checked
		$(document)
			.on('submit', 'form', (event) => {
				event.preventDefault();
				const $form = $(event.target);
				const setOk = () => {
					$('.js-message-ok')
						.removeClass('u-hide')
						.addClass('u-show');
					$('.js-content').hide();
					$form
						.find('.message--error')
						.removeClass('u-show')
						.addClass('u-hide');
				};
				const setFail = () => {
					$('.js-message-ok')
						.removeClass('u-show')
						.addClass('u-hide');
					$form
						.find('.message--error')
						.removeClass('u-hide')
						.addClass('u-show');
				};

				$.ajax({
					url: $form.attr('action'),
					data: $form.serialize(),
					dataType: 'jsonp',
					error: setFail,
					success: (data) => {
						if (data.result === 'error') {
							setFail();
							return;
						}
						setOk();
						$form[0].reset();
					},
				});
			})
			.on('click', '.js-again', (event) => {
				event.preventDefault();
				$('.js-content').show();
				$('#newsletter').val('');
				$('.js-message-ok')
					.removeClass('u-show')
					.addClass('u-hide');
				$('.message--error')
					.removeClass('u-show')
					.addClass('u-hide');
			});
	},

	initComponent(component) {
		return component.init();
	},
};
