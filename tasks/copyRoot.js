const { src, dest } = require('gulp');

const config = require('./helpers/getConfig.js');

module.exports = function copyRoot() {
	return src(['**/*', '!css/**/*', '!js/**/*', '!img/**/*', '!tpl/**/*'], {
		cwd: config.basePath.src,
	}).pipe(dest(config.basePath.dest));
};
